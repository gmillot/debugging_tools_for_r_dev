Download the desired Tagged version, never the current master.

Source the cute_little_functions.R into R/RStudio to have the functions available in the working environment.


WHAT'S NEW IN 

v1.8:

1) Updated for saferDev

v1.7:

1) Debugged


v1.6:

1) Debugged


v1.5:

1) Improved


v1.4:

1) Debugged


v1.3:

1) NULL arguments, non NULL argument, logical and NA arguments added


v1.2:

1) Printing result improvement

